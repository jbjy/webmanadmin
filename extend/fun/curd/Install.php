<?php
/**
 * FunAdmin
 * ============================================================================
 * 版权所有 2017-2028 FunAdmin，并保留所有权利。
 * 网站地址: https://www.FunAdmin.com
 * ----------------------------------------------------------------------------
 * 采用最新Thinkphp6实现
 * ============================================================================
 * Author: yuege
 * Date: 2020/9/21
 */

namespace fun\curd;

use Symfony\Component\Console\Question\Question;
use think\facade\Cache;
use think\facade\Db;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class Install extends Command
{
    //安装文件
    protected $lockFile;
    //数据库
    protected $databaseConfigFile;
    //sql 文件
    protected $sqlFile = '';
    //mysql版本
    protected $mysqlVersion = '5.7';
    //database模板
    protected $databaseTpl = '';
    protected static $defaultName = 'install';

    protected static $defaultDescription = '安装';

    protected function configure()
    {
        $db["database"] = config('thinkorm');
        $default = $db["database"]['default'];
        $config = $db["database"]['connections'][$default];
        $this->setName('install')
            ->addOption('hostname', 'm', InputOption::VALUE_OPTIONAL, 'hostname', $config['hostname'])
            ->addOption('hostport', 'r', InputOption::VALUE_OPTIONAL, 'hostport', $config['hostport'])
            ->addOption('database', 'd', InputOption::VALUE_OPTIONAL, 'database', $config['database'])
            ->addOption('charset', 'c', InputOption::VALUE_OPTIONAL, 'database', $config['charset'])
            ->addOption('prefix', 'x', InputOption::VALUE_OPTIONAL, 'prefix', $config['prefix'])
            ->addOption('username', 'u', InputOption::VALUE_OPTIONAL, 'mysql username', $config['username'])
            ->addOption('password', 'p', InputOption::VALUE_OPTIONAL, 'mysql password', $config['password'])
            ->addOption('force', 'f', InputOption::VALUE_OPTIONAL, 'force override', false)
            ->setDescription('FunAdmin install command');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->databaseConfigFile = config_path() . "/thinkorm.php";
        $this->sqlFile = app_path() . "/install/funadmin.sql";
        $this->lockFile = public_path() . "/install.lock";
        $this->databaseTpl = app_path()  . "/install/view/tpl/database.tpl";
        $force = $input->getOption('force');
        $this->lockFile = public_path() . "/install.lock";
        if (is_file($this->lockFile) && !$force) {
            $io->info("已经安装了,如需重新安装请输入 -f 1或 --force 1");
            return self::SUCCESS;
        }
        $this->detectionEnv($input,$output);
        $this->install($input,$output);
        return self::SUCCESS;
    }
    /**
     * 环境检测
     *
     * @time 2019年11月29日
     * @return void
     */
    protected function detectionEnv($input,$output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->info('environment begin to check...');
        if (version_compare(PHP_VERSION, '7.2.0', '<')) {
            $io->error('php version should >= 7.2.0');
            exit();
        }
        $io->info('php version ' . PHP_VERSION);
        if (!function_exists('session_start')) {
            $io->error('session extension not install');
            exit();
        }
        $io->info('session extension is installed');
        if (!function_exists('curl_exec')) {
            $io->error('curl extension not install');
            exit();
        }
        $io->info('curl extension is installed');
//
//        if (!extension_loaded('fileinfo')) {
//            $output->writeln('fileinfo extension not install');
//            exit();
//        }
//        $output->writeln('fileinfo extension is installed');

        if (!extension_loaded('openssl')) {
            $io->error('openssl extension not install');
            exit();
        }
        $io->info('openssl extension is installed');

        if (!extension_loaded('pdo')) {
            $io->error('pdo extension not install');
            exit();
        }
        $io->info('pdo extension is installed');

        if (!is_writable(base_path().'/runtime')) {
            $io->error('runtime path is  not writeable');
            exit();
        }

        $io->info('runtime  is witeable');

        //检测能否读取安装文件
        $sql = @file_get_contents($this->sqlFile);
        if (!$sql) {
            $io->error("Unable to read `{$this->sqlFile}`，Please check if you have read permission");
            exit();
        }

        $io->info('sql file is witeable');

        $io->success('🎉 environment checking finished');


    }
    /**
     * 开始安装
     * @return void
     */
    protected function install($input,$output){
        $env = base_path() . '/.env';
        $db["host"] = $input->getOption('hostname');
        $db["port"] = $input->getOption('hostport');
        $db["database"] = $input->getOption('database');
        $db["charset"] = $input->getOption('charset');
        $db["username"] =$input->getOption('username');
        $db["password"] = $input->getOption('password');
        $db["prefix"] = $input->getOption('prefix');
        if(file_exists($env)){
            $env = \parse_ini_file($env, true);
            $db["host"] =  $env['DB_HOST'] ;
            $db["port"] = $env['DB_PORT']  ;
            $db["database"] = $env['DB_NAME'] ;
            $db["charset"] = $env['DB_CHARSET']  ;
            $db["prefix"] = $env['DB_PREFIX']  ;
            $db["username"] = $env['DB_USER']  ;
            $db["password"] = $env['DB_PASSWORD']  ;
        }
        $io = new SymfonyStyle($input, $output);
        $db["host"] = $io->ask('👉 Set mysql hostname',$db["host"]);
        $db["port"] =  $io->ask('👉 Set mysql hostport','3306');
        $db["database"] =  $io->ask('👉 Set mysql database default (funadmin)',$db["database"]);
        $db['username'] = $io->ask('👉 Set mysql username default (root)',$db["username"]);
        $db['password'] = $io->ask('👉 Set mysql password required',$db["password"]);
        $db["prefix"] = $io->ask('👉 Set mysql table prefix default (fun_)',$db["prefix"]);
        $db["charset"] = $io->ask('👉 Set mysql table charset default (utf8mb4)',$db["charset"]);
        $admin["username"]  = $io->ask('👉 Set admin username required default (admin)','admin');
        $admin["password"] = $io->ask('👉 Set admin password required default (123456)','123456');
        $admin["rePassword"]  = $io->ask('👉 Set admin repeat password default (123456)','123456');
        $admin["email"] = $io->ask('👉 Set admin repeat password default (admin@admin.com)','admin@admin.com');
        if(!$admin["username"] || !$admin['rePassword'] ){
            $io->info('请输入管理员帐号和密码');
            while (!$admin["username"]) {
                $admin["username"] =  $io->ask('👉 请输入管理员账号: ');
                if ($admin["username"]) {
                    break;
                }
            }
            while (!$admin['rePassword']) {
                $admin["rePassword"] = $io->ask('👉 请输入管理员密码重复: ');
                if ($admin["rePassword"]) {
                    break;
                }
            }
            return self::FAILURE;
        }
        if (!preg_match("/^\w+$/", $admin["username"]) || strlen($admin["username"]) < 3 || strlen($admin["username"]) > 26) {
            $io->error('管理员用户名只能输入字母、数字、下划线！用户名请输入3~26位字符！');
            while (!$admin["username"]) {
                $admin["username"] =  $io->ask('👉 请输入管理员账号: ');
                if ($admin["username"]) {
                    break;
                }
            }
        }
        if(!preg_match('/^[0-9a-z_$]{6,16}$/i', $admin['password']) || strlen($admin['password']) < 5 || strlen($admin['password']) > 16){
            $io->info('管理员密码必须6-16位,且必须包含字母和数字,不能有中文和空格');
            while (!$admin["password"] ) {
                $admin["password"] = $io->ask('👉 请输入管理员密码: ');
                if ($admin["password"] ) {
                    break;
                }
            }
        }
        //判断两次输入是否一致
        if ($admin['password'] != $admin['rePassword']) {
            $io->info('管理员登录密码两次输入不一致！');
            while ($admin['password'] != $admin['rePassword']) {
                $admin["password"] = $io->ask('👉 请输入管理员密码: ');
                $admin["rePassword"] = $io->ask('👉 请输入重复密码: ');
                if ($admin['password'] == $admin['rePassword']) {
                    break;
                }
            }
        }
        try {
            $io->info('连接数据库...');
            // 连接数据库
            $link = @new \mysqli("{$db['host']}:{$db['port']}", $db['username'], $db['password']);
            if (mysqli_connect_errno()) {
                $io->warning("数据库链接失败:".mysqli_connect_errno());
                return self::FAILURE;
            }
            $link->query("SET NAMES 'utf8mbInputOption::VALUE_OPTIONAL'");
            if ($link->server_info < $this->mysqlVersion) {
                $io->error("MySQL数据库版本不能低于{$this->mysqlVersion},请将您的MySQL升级到{$this->mysqlVersion}及以上");
                return self::FAILURE;
            }
            // 创建数据库并选中
            if (!$link->select_db($db['database'])) {
                $create_sql = 'CREATE DATABASE IF NOT EXISTS ' . $db['database'] . ' DEFAULT CHARACTER SET '. $db["charset"].';';
                $link->query($create_sql) or  $io->warning('创建数据库失败');
                $link->select_db($db['database']);
            }
            $link->query("USE `{$db['database']}`");//使用数据库
            // 写入数据库
            $io->info('安装数据库中...');
            $sql = file_get_contents($this->sqlFile);
            $sql = str_replace(["`fun_",'CREATE TABLE'], ["`{$db['prefix']}",'CREATE TABLE IF NOT EXISTS'], $sql);
            $config = config('thinkorm');
            $config['connections']['mysql'] = [
                'type'      => 'mysql',
                'hostname'  => $db['host'],
                'database'  => $db['database'],
                'username'  => $db['username'],
                'password'  => $db['password'],
                'hostport'  => $db['port'],
                'prefix'    => $db['prefix'],
                'params'    => [],
                'charset'   => $db["charset"],
            ];
            try {
                Db::setConfig($config);
                $instance = Db::connect();
                $instance->execute("SELECT 1");     //如果是【数据】增删改查直接运行
                $instance->getPdo()->exec($sql);
                sleep(2);
                $password = password_hash($admin['password'], PASSWORD_BCRYPT);
                $instance->execute("UPDATE {$db['prefix']}admin SET `email`='{$admin['email']}',`username` = '{$admin['username']}',`password` = '{$password}' WHERE `username` = 'admin'");
                $instance->execute("UPDATE {$db['prefix']}member SET `email`='{$admin['email']}',`username` = '{$admin['username']}',`password` = '{$password}' WHERE `username` = 'admin'");
            } catch (\PDOException $e) {
                $io->error($e->getMessage());
                return self::FAILURE;
            }catch(\Exception $e){
                $io->error($e->getMessage());
                return self::FAILURE;

            }
            $io->info('数据库安装完成...');
            $databaseTpl = @file_get_contents($this->databaseTpl);
            $io->info('修改数据配置中...');

            //替换数据库相关配置
            $putDatabase = str_replace(
                ['{{hostname}}', '{{database}}', '{{username}}', '{{password}}', '{{port}}', '{{prefix}}'],
                [$db['host'],$db['database'], $db['username'], $db['password'], $db['port'], $db['prefix']],
                file_get_contents($this->databaseTpl));
            $putConfig = @file_put_contents($this->databaseConfigFile, $putDatabase);
            if (!$putConfig) {
                $io->warning('安装失败，请确认database.php有写权限！');
                return self::FAILURE;
            }
            $adminUser['username'] = $admin['username'];
            $adminUser['password'] = $admin['password'];
            $io->success('👉 恭喜您：系统已经安装完成... 通过域名+后台入口文件即可访问后台');
            $io->success('👉 管理员账号: '.$adminUser["username"].'，管理员密码:'.$adminUser['password'].',后台入口:域名+/backend');
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            return self::FAILURE;

        }
        return self::SUCCESS;
    }
}
