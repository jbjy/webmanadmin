<?php
/**
 * +
 * | 后台中间件验证权限
 */

namespace app\backend\middleware;

use app\backend\service\AuthService;
use Webman\Http\Request;
use Webman\Http\Response;
use Webman\MiddlewareInterface;

class CheckRole implements MiddlewareInterface
{
    public function process(Request $request, callable $handler): Response
    {

        $auth = new AuthService();
        $response = $auth->checkNode();
        if($response!==true && !is_null($response)){
            return $response;
        }
        return $handler($request);
    }


}